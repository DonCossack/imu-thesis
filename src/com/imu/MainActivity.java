package com.imu;

import com.example.imu.R;

import android.app.Activity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements SensorEventListener {
	SensorManager msensorManager = null;
	private static final float NS2S = 1.0f / 1000000000.0f;
	
	//Array to store gyro data
	public List<Float> gyroPitcharray=new ArrayList<Float>();
	public List<Float> gyroAzimutharray=new ArrayList<Float>();
	public List<Float> gyroRollarray=new ArrayList<Float>();
	//Array to store Accelero data
	public List<Float> AccPitcharray=new ArrayList<Float>();
	public List<Float> AccAzimutharray=new ArrayList<Float>();
	public List<Float> AccRollarray=new ArrayList<Float>();
	//Array to store Kalman data
	public List<Double> KalPitcharray=new ArrayList<Double>();
	public List<Double> KalAzimutharray=new ArrayList<Double>();
	public List<Double> KalRollarray=new ArrayList<Double>();
	
	private float timestamp;
	private boolean initState = true;
	public static final float EPSILON = 0.000000001f;
	public KalmanFusion filter1=new KalmanFusion();
	public KalmanFusion filter2=new KalmanFusion();
	public KalmanFusion filter3=new KalmanFusion();
	private double bias_pitch=0.5;
	private double bias_azimuth=0.5;
	private double bias_roll=0.5;
	// orientation angles from accel and magnet
	private float[] accMagOrientation = new float[3];
	
	//Kalman paratmeter initilization
	private double theta_init_pitch=0;
	private double theta_init_azimuth=0;
	private double theta_init_roll=0;
	double[][] P = new double[2][2];
	float dT=0;
	boolean startfilter=false;

	// Accelero
	TextView outputX;
	TextView outputY;
	TextView outputZ;

	// for orientation values
	TextView outputX2;
	TextView outputY2;
	TextView outputZ2;
	
	//Kalman value
	
	TextView outputX3;
	TextView outputY3;
	TextView outputZ3;
	// angular speeds from gyro
	private float[] gyro = new float[3];

	// rotation matrix from gyro data
	private float[] gyroMatrix = new float[9];

	// orientation angles from gyro matrix
	private float[] gyroOrientation = new float[3];

	// magnetic field vector
	private float[] magnet = new float[3];

	// accelerometer vector
	private float[] accel = new float[3];
	// accelerometer and magnetometer based rotation matrix
	private float[] rotationMatrix = new float[9];

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		msensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		initListeners();
		// just some textviews, for data output
		outputX = (TextView) findViewById(R.id.TextView01);
		outputY = (TextView) findViewById(R.id.TextView02);
		outputZ = (TextView) findViewById(R.id.TextView03);

		outputX2 = (TextView) findViewById(R.id.TextView04);
		outputY2 = (TextView) findViewById(R.id.TextView05);
		outputZ2 = (TextView) findViewById(R.id.TextView06);
		
		outputX3 = (TextView) findViewById(R.id.TextView07);
		outputY3 = (TextView) findViewById(R.id.TextView08);
		outputZ3 = (TextView) findViewById(R.id.TextView09);
		
		gyroOrientation[0] = 0.0f;
		gyroOrientation[1] = 0.0f;
		gyroOrientation[2] = 0.0f;

		// initialise gyroMatrix with identity matrix
		gyroMatrix[0] = 1.0f;
		gyroMatrix[1] = 0.0f;
		gyroMatrix[2] = 0.0f;
		gyroMatrix[3] = 0.0f;
		gyroMatrix[4] = 1.0f;
		gyroMatrix[5] = 0.0f;
		gyroMatrix[6] = 0.0f;
		gyroMatrix[7] = 0.0f;
		gyroMatrix[8] = 1.0f;

		// get sensorManager and initialise sensor listeners

	}

	public void initListeners() {
		msensorManager.registerListener(this,
				msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);

		msensorManager.registerListener(this,
				msensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
				SensorManager.SENSOR_DELAY_NORMAL);

		msensorManager.registerListener(this,
				msensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			// copy new accelerometer data into accel array
			// then calculate new orientation
			System.arraycopy(event.values, 0, accel, 0, 3);
			
			if (SensorManager.getRotationMatrix(rotationMatrix, null, accel,
					magnet)) {
				SensorManager.getOrientation(rotationMatrix, accMagOrientation);
				AccPitcharray.add(gyroOrientation[1]);
				AccAzimutharray.add(gyroOrientation[0]);
				AccRollarray.add(gyroOrientation[2]);
				
				outputX.setText(String.valueOf("Accelero Pitch:"+accMagOrientation[1]));
				outputY.setText(String.valueOf("Accelero Roll:"+accMagOrientation[2]));
				outputZ.setText(String.valueOf("Accelero Azimuth:"+accMagOrientation[0]));
				
			}
			break;

		case Sensor.TYPE_GYROSCOPE:
			// process gyro data
			 gyroFunction(event);
			 
			break;

		case Sensor.TYPE_MAGNETIC_FIELD:
			// copy new magnetometer data into magnet array
			System.arraycopy(event.values, 0, magnet, 0, 3);
			break;
		}
	}
	
	public void saveacclog(View v){
		savetofile(AccPitcharray.toString(),"acc_pitch.txt");
		savetofile(AccRollarray.toString(),"acc_roll.txt");
		savetofile(AccAzimutharray.toString(),"acc_azimuth.txt");
		
	}

	private float[] getRotationMatrixFromOrientation(float[] Orientation) {
		float[] xM = new float[9];
		float[] yM = new float[9];
		float[] zM = new float[9];

		float sinX = (float) Math.sin(Orientation[1]);
		float cosX = (float) Math.cos(Orientation[1]);
		float sinY = (float) Math.sin(Orientation[2]);
		float cosY = (float) Math.cos(Orientation[2]);
		float sinZ = (float) Math.sin(Orientation[0]);
		float cosZ = (float) Math.cos(Orientation[0]);

		// rotation about x-axis (pitch)
		xM[0] = 1.0f;
		xM[1] = 0.0f;
		xM[2] = 0.0f;
		xM[3] = 0.0f;
		xM[4] = cosX;
		xM[5] = sinX;
		xM[6] = 0.0f;
		xM[7] = -sinX;
		xM[8] = cosX;

		// rotation about y-axis (roll)
		yM[0] = cosY;
		yM[1] = 0.0f;
		yM[2] = sinY;
		yM[3] = 0.0f;
		yM[4] = 1.0f;
		yM[5] = 0.0f;
		yM[6] = -sinY;
		yM[7] = 0.0f;
		yM[8] = cosY;

		// rotation about z-axis (azimuth)
		zM[0] = cosZ;
		zM[1] = sinZ;
		zM[2] = 0.0f;
		zM[3] = -sinZ;
		zM[4] = cosZ;
		zM[5] = 0.0f;
		zM[6] = 0.0f;
		zM[7] = 0.0f;
		zM[8] = 1.0f;

		// rotation order is y, x, z (roll, pitch, azimuth)
		float[] resultMatrix = matrixMultiplication(xM, yM);
		resultMatrix = matrixMultiplication(zM, resultMatrix);
		return resultMatrix;
	}

	public void gyroFunction(SensorEvent event) {
		// don't start until first accelerometer/magnetometer orientation has
		// been acquired
		if (accMagOrientation == null)
			return;

		// initialisation of the gyroscope based rotation matrix
		if (initState) {
			float[] initMatrix = new float[9];
			initMatrix = getRotationMatrixFromOrientation(accMagOrientation);
			
			float[] test = new float[3];
			SensorManager.getOrientation(initMatrix, test);
			gyroMatrix = matrixMultiplication(gyroMatrix, initMatrix);
			//after make sure both accelero and gyro are initiated, initiate value for Kalman filter
			//do this only once
		/*	theta_init_pitch=accMagOrientation[1];
			theta_init_azimuth=accMagOrientation[0];
			theta_init_roll=accMagOrientation[2];
			initState = false;*/
		}

		// copy the new gyro values into the gyro array
		// convert the raw gyro data into a rotation vector
		float[] deltaVector = new float[4];
		if (timestamp != 0) {
			dT = (event.timestamp - timestamp) * NS2S;
			System.arraycopy(event.values, 0, gyro, 0, 3);
			getRotationVectorFromGyro(gyro, deltaVector, dT / 2.0f);
		}

		// measurement done, save current time for next interval
		timestamp = event.timestamp;

		// convert rotation vector into rotation matrix
		float[] deltaMatrix = new float[9];
		SensorManager.getRotationMatrixFromVector(deltaMatrix, deltaVector);

		// apply the new rotation interval on the gyroscope based rotation
		// matrix
		gyroMatrix = matrixMultiplication(gyroMatrix, deltaMatrix);

		// get the gyroscope based orientation from the rotation matrix
		SensorManager.getOrientation(gyroMatrix, gyroOrientation);
		outputX2.setText(String.valueOf("Gyro Pitch:"+gyroOrientation[1]));
		outputY2.setText(String.valueOf("Gyro Roll:"+gyroOrientation[2]));
		outputZ2.setText(String.valueOf("Gyro Azimuth:"+gyroOrientation[0]));
	
		gyroPitcharray.add(gyroOrientation[1]);
		gyroAzimutharray.add(gyroOrientation[0]);
		gyroRollarray.add(gyroOrientation[2]);
		FilterPitch();
		FilterAzimuth();
		FilterRoll();
		
		startfilter=true;
	}
public void savegyrolog(View v)
{	savetofile(gyroPitcharray.toString(),"gyro_pitch.txt");
	savetofile(gyroAzimutharray.toString(),"gyro_azimuth.txt");
	savetofile(gyroRollarray.toString(),"gyro_roll.txt");
	

	
}

public void savekallog(View v)
{	savetofile(KalPitcharray.toString(),"kal_pitch.txt");
	savetofile(KalAzimutharray.toString(),"kal_azimuth.txt");
	savetofile(KalRollarray.toString(),"kal_roll.txt");
	

	
}


	private float[] matrixMultiplication(float[] A, float[] B) {
		float[] result = new float[9];

		result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
		result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
		result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];

		result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
		result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
		result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];

		result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
		result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
		result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];

		return result;
	}

	private void getRotationVectorFromGyro(float[] gyroValues,
			float[] deltaRotationVector, float timeFactor) {
		float[] normValues = new float[3];

		// Calculate the angular speed of the sample
		float omegaMagnitude = (float) Math
				.sqrt(gyroValues[0] * gyroValues[0] + gyroValues[1]
						* gyroValues[1] + gyroValues[2] * gyroValues[2]);

		// Normalize the rotation vector if it's big enough to get the axis
		if (omegaMagnitude > EPSILON) {
			normValues[0] = gyroValues[0] / omegaMagnitude;
			normValues[1] = gyroValues[1] / omegaMagnitude;
			normValues[2] = gyroValues[2] / omegaMagnitude;
		}

		// Integrate around this axis with the angular speed by the timestep
		// in order to get a delta rotation from this sample over the timestep
		// We will convert this axis-angle representation of the delta rotation
		// into a quaternion before turning it into the rotation matrix.
		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
		deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
		deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
		deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
		deltaRotationVector[3] = cosThetaOverTwo;
	}
private void savetofile(String str,String filename){
	
	File file = new File(Environment.getExternalStorageDirectory(), filename);
	FileOutputStream fos;
	byte[] data = str.getBytes();
	try {
	    fos = new FileOutputStream(file);
	    fos.write(data);
	    fos.flush();
	    fos.close();
	} catch (FileNotFoundException e) {
	    // handle exception
	} catch (IOException e) {
	    // handle exception
	}
	
	
}

private  void FilterPitch(){
	
	double theta_gyro=gyroOrientation[1];
	double theta_acc=accMagOrientation[1];
	double dt=dT / 2.0f;
	double[] result=filter1.filtertheta(theta_gyro, theta_acc, dt, bias_pitch,theta_init_pitch);
	theta_init_pitch=result[0];
	KalPitcharray.add(theta_init_pitch);
	bias_pitch=result[1];
	outputX3.setText("Kalman filtered pitch:"+String.valueOf(theta_init_pitch));
	
}
private  void FilterAzimuth(){
	
	double theta_gyro=gyroOrientation[0];
	double theta_acc=accMagOrientation[0];
	double dt=dT / 2.0f;
	double[] result=filter2.filtertheta(theta_gyro, theta_acc, dt, bias_azimuth,theta_init_azimuth);
	theta_init_azimuth=result[0];
	KalAzimutharray.add(theta_init_azimuth);
	outputY3.setText("Kalman filtered azimuth:"+String.valueOf(theta_init_azimuth));
	bias_azimuth=result[1];
}
private  void FilterRoll(){
	
	double theta_gyro=gyroOrientation[2];
	double theta_acc=accMagOrientation[2];
	double dt=dT / 2.0f;
	double[] result=filter3.filtertheta(theta_gyro, theta_acc, dt, bias_roll,theta_init_roll);
	theta_init_roll=result[0];
	KalRollarray.add(theta_init_roll);
	outputZ3.setText("Kalman filtered roll:"+String.valueOf(theta_init_roll));
	bias_roll=result[1];
}
}

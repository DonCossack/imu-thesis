package com.imu;

public class KalmanFusion {
	private double Q_angle = 0.005;
	private double Q_gyroBias = 0.05;
	private double R_measure = 0.04;
	public double[][] P=new double[2][2];

	public double[] filtertheta(double theta_gyro_k, double theta_acc_k,
			double dt,double bias,double theta_k_1) {

		double angle_k = theta_k_1 + (theta_gyro_k - bias) * dt;
		// Innovation error
		double error = theta_acc_k - angle_k;
		// Gain
		double[] K = gaincalculation(error, dt);
		angle_k = angle_k + K[0] * error;
		bias = bias + K[1] * error;
		//update P
		P[0][0] -= K[0] * P[0][0];
        P[0][1] -= K[0] * P[0][1];
        P[1][0] -= K[1] * P[0][0];
        P[1][1] -= K[1] * P[0][1];
		// return filtered result, in which 1st element is angle and 2nd element
		// is bias value
		double[] resultarray = new double[3];
		resultarray[0] = angle_k;
		resultarray[1] = bias;
		return resultarray;

	}
	
	
	
	

	// Kalman gain calculation
	private double[] gaincalculation(double error, double dt) {
		// Calculate Pk-k-1 from priori error covariance matrix P
		
		P[0][0] += dt * (dt * P[1][1] - P[0][1] - P[1][0] + Q_angle);
		P[0][1] -= dt * P[1][1];
		P[1][0] -= dt * P[1][1];
		P[1][1] += Q_gyroBias * dt;
		double[] K = new double[2];
		K[0] = P[0][0] / (P[0][0] + R_measure);
		K[1] = P[0][1] / (P[0][0] + R_measure);
		return K;
	}

}
